﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainWindow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainWindow))
        Me.menuStrip = New System.Windows.Forms.MenuStrip()
        Me.fileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.fileMenuOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.fileMenuNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.fileMenuSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.fileMenuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolStrip = New System.Windows.Forms.ToolStrip()
        Me.toolStripFirst = New System.Windows.Forms.ToolStripButton()
        Me.toolStripPrevious = New System.Windows.Forms.ToolStripButton()
        Me.toolStripNext = New System.Windows.Forms.ToolStripButton()
        Me.toolStripLast = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.toolStripSearch = New System.Windows.Forms.ToolStripButton()
        Me.toolStripEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripAdd = New System.Windows.Forms.ToolStripButton()
        Me.toolStripDelete = New System.Windows.Forms.ToolStripButton()
        Me.titleLabel = New System.Windows.Forms.Label()
        Me.dateLabel = New System.Windows.Forms.Label()
        Me.textBoxTitle = New System.Windows.Forms.TextBox()
        Me.textBoxMemo = New System.Windows.Forms.TextBox()
        Me.dateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.statusStrip = New System.Windows.Forms.StatusStrip()
        Me.statusStripLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.checkboxDeleted = New System.Windows.Forms.CheckBox()
        Me.menuStrip.SuspendLayout()
        Me.toolStrip.SuspendLayout()
        Me.statusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'menuStrip
        '
        Me.menuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.fileMenu})
        Me.menuStrip.Location = New System.Drawing.Point(0, 0)
        Me.menuStrip.Name = "menuStrip"
        Me.menuStrip.Size = New System.Drawing.Size(763, 43)
        Me.menuStrip.TabIndex = 0
        Me.menuStrip.Text = "MenuStrip1"
        '
        'fileMenu
        '
        Me.fileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.fileMenuOpen, Me.fileMenuNew, Me.fileMenuSave, Me.fileMenuExit})
        Me.fileMenu.Name = "fileMenu"
        Me.fileMenu.Size = New System.Drawing.Size(64, 39)
        Me.fileMenu.Text = "File"
        '
        'fileMenuOpen
        '
        Me.fileMenuOpen.Name = "fileMenuOpen"
        Me.fileMenuOpen.Size = New System.Drawing.Size(152, 40)
        Me.fileMenuOpen.Text = "Open"
        '
        'fileMenuNew
        '
        Me.fileMenuNew.Name = "fileMenuNew"
        Me.fileMenuNew.Size = New System.Drawing.Size(152, 40)
        Me.fileMenuNew.Text = "New"
        '
        'fileMenuSave
        '
        Me.fileMenuSave.Name = "fileMenuSave"
        Me.fileMenuSave.Size = New System.Drawing.Size(152, 40)
        Me.fileMenuSave.Text = "Save"
        '
        'fileMenuExit
        '
        Me.fileMenuExit.Name = "fileMenuExit"
        Me.fileMenuExit.Size = New System.Drawing.Size(152, 40)
        Me.fileMenuExit.Text = "Exit"
        '
        'toolStrip
        '
        Me.toolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripFirst, Me.toolStripPrevious, Me.toolStripNext, Me.toolStripLast, Me.toolStripSeparator, Me.toolStripSearch, Me.toolStripEdit, Me.toolStripAdd, Me.toolStripDelete})
        Me.toolStrip.Location = New System.Drawing.Point(0, 43)
        Me.toolStrip.Name = "toolStrip"
        Me.toolStrip.Size = New System.Drawing.Size(763, 58)
        Me.toolStrip.TabIndex = 1
        Me.toolStrip.Text = "ToolStrip"
        '
        'toolStripFirst
        '
        Me.toolStripFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolStripFirst.Image = CType(resources.GetObject("toolStripFirst.Image"), System.Drawing.Image)
        Me.toolStripFirst.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripFirst.Name = "toolStripFirst"
        Me.toolStripFirst.Size = New System.Drawing.Size(23, 55)
        Me.toolStripFirst.Text = "First"
        '
        'toolStripPrevious
        '
        Me.toolStripPrevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolStripPrevious.Image = CType(resources.GetObject("toolStripPrevious.Image"), System.Drawing.Image)
        Me.toolStripPrevious.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripPrevious.Name = "toolStripPrevious"
        Me.toolStripPrevious.Size = New System.Drawing.Size(23, 55)
        Me.toolStripPrevious.Text = "Previous"
        '
        'toolStripNext
        '
        Me.toolStripNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolStripNext.Image = CType(resources.GetObject("toolStripNext.Image"), System.Drawing.Image)
        Me.toolStripNext.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripNext.Name = "toolStripNext"
        Me.toolStripNext.Size = New System.Drawing.Size(23, 55)
        Me.toolStripNext.Text = "Next"
        '
        'toolStripLast
        '
        Me.toolStripLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolStripLast.Image = CType(resources.GetObject("toolStripLast.Image"), System.Drawing.Image)
        Me.toolStripLast.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripLast.Name = "toolStripLast"
        Me.toolStripLast.Size = New System.Drawing.Size(23, 55)
        Me.toolStripLast.Text = "Last"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 58)
        '
        'toolStripSearch
        '
        Me.toolStripSearch.Image = CType(resources.GetObject("toolStripSearch.Image"), System.Drawing.Image)
        Me.toolStripSearch.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripSearch.Name = "toolStripSearch"
        Me.toolStripSearch.Size = New System.Drawing.Size(93, 55)
        Me.toolStripSearch.Text = "Search"
        Me.toolStripSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripEdit
        '
        Me.toolStripEdit.Image = CType(resources.GetObject("toolStripEdit.Image"), System.Drawing.Image)
        Me.toolStripEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripEdit.Name = "toolStripEdit"
        Me.toolStripEdit.Size = New System.Drawing.Size(61, 55)
        Me.toolStripEdit.Text = "Edit"
        Me.toolStripEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripAdd
        '
        Me.toolStripAdd.Image = CType(resources.GetObject("toolStripAdd.Image"), System.Drawing.Image)
        Me.toolStripAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripAdd.Name = "toolStripAdd"
        Me.toolStripAdd.Size = New System.Drawing.Size(65, 55)
        Me.toolStripAdd.Text = "Add"
        Me.toolStripAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripDelete
        '
        Me.toolStripDelete.Image = CType(resources.GetObject("toolStripDelete.Image"), System.Drawing.Image)
        Me.toolStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripDelete.Name = "toolStripDelete"
        Me.toolStripDelete.Size = New System.Drawing.Size(90, 55)
        Me.toolStripDelete.Text = "Delete"
        Me.toolStripDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'titleLabel
        '
        Me.titleLabel.AutoSize = True
        Me.titleLabel.Location = New System.Drawing.Point(12, 137)
        Me.titleLabel.Name = "titleLabel"
        Me.titleLabel.Size = New System.Drawing.Size(52, 26)
        Me.titleLabel.TabIndex = 2
        Me.titleLabel.Text = "Title"
        '
        'dateLabel
        '
        Me.dateLabel.AutoSize = True
        Me.dateLabel.Location = New System.Drawing.Point(13, 193)
        Me.dateLabel.Name = "dateLabel"
        Me.dateLabel.Size = New System.Drawing.Size(58, 26)
        Me.dateLabel.TabIndex = 3
        Me.dateLabel.Text = "Date"
        '
        'textBoxTitle
        '
        Me.textBoxTitle.Location = New System.Drawing.Point(121, 131)
        Me.textBoxTitle.Name = "textBoxTitle"
        Me.textBoxTitle.Size = New System.Drawing.Size(392, 31)
        Me.textBoxTitle.TabIndex = 4
        '
        'textBoxMemo
        '
        Me.textBoxMemo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.textBoxMemo.Location = New System.Drawing.Point(12, 259)
        Me.textBoxMemo.Multiline = True
        Me.textBoxMemo.Name = "textBoxMemo"
        Me.textBoxMemo.Size = New System.Drawing.Size(739, 275)
        Me.textBoxMemo.TabIndex = 5
        '
        'dateTimePicker
        '
        Me.dateTimePicker.Location = New System.Drawing.Point(121, 187)
        Me.dateTimePicker.Name = "dateTimePicker"
        Me.dateTimePicker.Size = New System.Drawing.Size(392, 31)
        Me.dateTimePicker.TabIndex = 6
        '
        'statusStrip
        '
        Me.statusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statusStripLabel})
        Me.statusStrip.Location = New System.Drawing.Point(0, 558)
        Me.statusStrip.Name = "statusStrip"
        Me.statusStrip.Size = New System.Drawing.Size(763, 40)
        Me.statusStrip.TabIndex = 7
        Me.statusStrip.Text = "StatusStrip"
        '
        'statusStripLabel
        '
        Me.statusStripLabel.Name = "statusStripLabel"
        Me.statusStripLabel.Size = New System.Drawing.Size(82, 35)
        Me.statusStripLabel.Text = "Status"
        '
        'checkboxDeleted
        '
        Me.checkboxDeleted.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.checkboxDeleted.AutoSize = True
        Me.checkboxDeleted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.checkboxDeleted.Location = New System.Drawing.Point(638, 131)
        Me.checkboxDeleted.Name = "checkboxDeleted"
        Me.checkboxDeleted.Size = New System.Drawing.Size(113, 30)
        Me.checkboxDeleted.TabIndex = 8
        Me.checkboxDeleted.Text = "Deleted"
        Me.checkboxDeleted.UseVisualStyleBackColor = True
        '
        'MainWindow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 598)
        Me.Controls.Add(Me.checkboxDeleted)
        Me.Controls.Add(Me.statusStrip)
        Me.Controls.Add(Me.dateTimePicker)
        Me.Controls.Add(Me.textBoxMemo)
        Me.Controls.Add(Me.textBoxTitle)
        Me.Controls.Add(Me.dateLabel)
        Me.Controls.Add(Me.titleLabel)
        Me.Controls.Add(Me.toolStrip)
        Me.Controls.Add(Me.menuStrip)
        Me.MinimumSize = New System.Drawing.Size(789, 667)
        Me.Name = "MainWindow"
        Me.Text = "Memory Helper"
        Me.menuStrip.ResumeLayout(False)
        Me.menuStrip.PerformLayout()
        Me.toolStrip.ResumeLayout(False)
        Me.toolStrip.PerformLayout()
        Me.statusStrip.ResumeLayout(False)
        Me.statusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents menuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents fileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents toolStripFirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripPrevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripNext As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents toolStripSearch As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents fileMenuOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fileMenuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fileMenuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fileMenuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents titleLabel As System.Windows.Forms.Label
    Friend WithEvents dateLabel As System.Windows.Forms.Label
    Friend WithEvents textBoxTitle As System.Windows.Forms.TextBox
    Friend WithEvents textBoxMemo As System.Windows.Forms.TextBox
    Friend WithEvents dateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents statusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents statusStripLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents checkboxDeleted As System.Windows.Forms.CheckBox

End Class
