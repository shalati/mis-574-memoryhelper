﻿Option Explicit On
Option Strict On

Imports System.IO

Public Class MainWindow

    Dim strFilePathName As String = ""
    Dim intCurrentRecord As Integer = -1
    Dim dataTable(-1) As MemoryRecord

    ''' <summary>
    ''' Enables editing of the current record.
    ''' </summary>
    Private Sub allowEditRecord()
        textBoxTitle.Enabled = True
        textBoxMemo.Enabled = True
        dateTimePicker.Enabled = True
        checkboxDeleted.Enabled = True
    End Sub

    ''' <summary>
    ''' Disables editing of the current record.
    ''' </summary>
    Private Sub protectRecord()
        textBoxTitle.Enabled = False
        textBoxMemo.Enabled = False
        dateTimePicker.Enabled = False
        checkboxDeleted.Enabled = False
    End Sub

    ''' <summary>
    ''' Clears the current record display.
    ''' </summary>
    Private Sub clearDisplay()
        textBoxTitle.Text = String.Empty
        textBoxMemo.Text = String.Empty
        dateTimePicker.Value = Today.Date
        checkboxDeleted.Checked = False
    End Sub

    ''' <summary>
    ''' Updates the status bar with current record information.
    ''' </summary>
    Private Sub updatePosition()
        statusStripLabel.Text = "Current Record = " & (intCurrentRecord + 1).ToString & " of " & dataTable.Length.ToString
    End Sub

    ''' <summary>
    ''' Creates a new record and sets it to be edited.
    ''' </summary>
    Private Sub newRecord()
        saveRecord()
        clearDisplay()
        ReDim Preserve dataTable(dataTable.Length)
        intCurrentRecord = dataTable.GetUpperBound(0)
        allowEditRecord()
        updatePosition()
    End Sub

    ''' <summary>
    ''' Marks the current record for deletion.
    ''' </summary>
    Private Sub deleteRecord()
        If intCurrentRecord >= 0 Then
            dataTable(intCurrentRecord).blnDeleted = True
            checkboxDeleted.Checked = True
            protectRecord()
        End If

    End Sub

    ''' <summary>
    ''' Saves the current record to the data table.
    ''' </summary>
    Private Sub saveRecord()
        If intCurrentRecord >= 0 Then
            dataTable(intCurrentRecord).strTitle = textBoxTitle.Text
            dataTable(intCurrentRecord).strMemo = textBoxMemo.Text
            dataTable(intCurrentRecord).blnDeleted = checkboxDeleted.Checked
            dataTable(intCurrentRecord).dDate = dateTimePicker.Value
            protectRecord()
        End If
    End Sub

    ''' <summary>
    ''' Empties out all the records in the data table.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub emptyRecords()
        clearDisplay()
        protectRecord()
        intCurrentRecord = -1
        ReDim dataTable(-1)
    End Sub

    ''' <summary>
    ''' Sets the form to the current record of the data table.
    ''' </summary>
    ''' <param name="index">Index of the data table.</param>
    Private Sub showRecord(ByVal index As Integer)
        saveRecord()
        If index >= 0 And index <= dataTable.GetUpperBound(0) Then
            ' Set the values in the form.
            Dim record As MemoryRecord = dataTable(index)
            textBoxTitle.Text = record.strTitle
            textBoxMemo.Text = record.strMemo
            dateTimePicker.Value = record.dDate
            checkboxDeleted.Checked = record.blnDeleted

            ' Update the index.
            intCurrentRecord = index

            ' Update our status.
            updatePosition()
        End If
    End Sub

    ''' <summary>
    ''' Finds a record with a given text in the Title or Memo.
    ''' </summary>
    ''' <param name="strSearchTerm">Text to search for.</param>
    ''' <returns>Returns the index of the found record, -1 if not found.</returns>
    Private Function findRecord(ByVal strSearchTerm As String) As Integer
        Dim retValue As Integer = -1
        Dim currentElement As Integer = 0

        ' Loop the Array, check the Memo or Title
        For Each item As MemoryRecord In dataTable
            If item.strMemo.Contains(strSearchTerm) Or item.strTitle.Contains(strSearchTerm) Then
                retValue = currentElement
                Exit For
            End If
            currentElement += 1
        Next item

        Return retValue
    End Function

    ''' <summary>
    ''' Saves the data table to a file.
    ''' </summary>
    ''' <param name="dataSource">Name of file to save.</param>
    ''' <param name="table">Table to be saved.</param>
    ''' <returns>True if the save was successful, false if not.</returns>
    Private Function saveTable(ByVal dataSource As String, ByVal table() As MemoryRecord) As Boolean
        Dim outputStream As StreamWriter = Nothing
        Dim blnSuccess As Boolean = True

        Try
            outputStream = New StreamWriter(dataSource, False)

            ' Loop through the items, save the ones not marked for deletion.
            For Each item As MemoryRecord In table
                If item.blnDeleted = False Then
                    outputStream.WriteLine(item.dDate)
                    outputStream.WriteLine(item.strTitle)
                    outputStream.WriteLine(item.strMemo)
                End If
            Next
        Catch ex As Exception
            blnSuccess = False
        Finally
            If Not IsNothing(outputStream) Then
                outputStream.Close()
            End If
        End Try

        Return blnSuccess
    End Function

    ''' <summary>
    ''' Opens a dialog to get a file from the user.
    ''' </summary>
    ''' <returns>The file selected by the user or nothing.</returns>
    Private Function getFile() As String
        Dim findFileDialog As New OpenFileDialog
        Dim result As DialogResult

        With findFileDialog
            .Title = "Locate Memory Helper Data File"
            .InitialDirectory = Directory.GetCurrentDirectory
            .FileName = ""

            .Filter = "TextFiles (*.txt)|*.txt"
            result = .ShowDialog
        End With

        If result <> DialogResult.Cancel Then
            Return findFileDialog.FileName
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Parses the data table array from a datasource file.
    ''' </summary>
    ''' <param name="dataSource">Data source pointing to a file.</param>
    ''' <param name="table">Table to be populated.</param>
    ''' <returns>Returns the table length after loading the file.</returns>
    Private Function fillTable(ByVal dataSource As String, ByRef table() As MemoryRecord) As Integer
        Dim newRecord As MemoryRecord
        Dim inputStream As StreamReader = Nothing

        Try
            inputStream = New StreamReader(dataSource)
            Do Until inputStream.EndOfStream
                ' Parse the new record.
                newRecord.dDate = Convert.ToDateTime(inputStream.ReadLine())
                newRecord.strTitle = inputStream.ReadLine()
                newRecord.strMemo = inputStream.ReadLine()
                newRecord.blnDeleted = False

                ' Add it to the array.
                ReDim Preserve table(table.Length)
                table(table.Length - 1) = newRecord
            Loop
        Catch ex As Exception
            MsgBox("Error Reading File", MsgBoxStyle.Critical)
        Finally
            If Not IsNothing(inputStream) Then
                inputStream.Close()
            End If
        End Try

        Return table.Length
    End Function

    ''' <summary>
    ''' Shows a dialog allowing the user to create a new file.
    ''' </summary>
    ''' <returns>Returns the new file name.</returns>
    Private Function newFile() As String
        Dim newFileDialog As New SaveFileDialog()

        With newFileDialog
            .Filter = "txt files (*.txt)|*.txt"
            .FilterIndex = 1
            .RestoreDirectory = True
            .DefaultExt = "txt"
        End With

        If newFileDialog.ShowDialog() = DialogResult.OK Then
            Return newFileDialog.FileName
        Else
            Return ""
        End If
    End Function

    ' --------------------------------------------------------------
    ' Event Handlers
    ' --------------------------------------------------------------

    ''' <summary>
    ''' Event handler for the Toolstrip Edit button.
    ''' </summary>
    Private Sub toolStripEdit_Click(sender As Object, e As EventArgs) Handles toolStripEdit.Click
        If intCurrentRecord < 0 Then
            newRecord()
        Else
            allowEditRecord()
        End If
    End Sub

    ''' <summary>
    ''' Event handler for the Toolstrip Add button.
    ''' </summary>
    Private Sub toolStripAdd_Click(sender As Object, e As EventArgs) Handles toolStripAdd.Click
        newRecord()
    End Sub

    ''' <summary>
    ''' Event handler for the Toolstrip Delete button.
    ''' </summary>
    Private Sub toolStripDelete_Click(sender As Object, e As EventArgs) Handles toolStripDelete.Click
        deleteRecord()
    End Sub

    ''' <summary>
    ''' Event handler for the Toolstrip First button.
    ''' </summary>
    Private Sub toolStripFirst_Click(sender As Object, e As EventArgs) Handles toolStripFirst.Click
        showRecord(0)
    End Sub

    ''' <summary>
    ''' Event handler for the Toolstrip Previous button.
    ''' </summary>
    Private Sub toolStripPrevious_Click(sender As Object, e As EventArgs) Handles toolStripPrevious.Click
        showRecord(intCurrentRecord - 1)
    End Sub

    ''' <summary>
    ''' Event handler for the Toolstrip Next button.
    ''' </summary>
    Private Sub toolStripNext_Click(sender As Object, e As EventArgs) Handles toolStripNext.Click
        showRecord(intCurrentRecord + 1)
    End Sub

    ''' <summary>
    ''' Event handler for the Toolstrip Last button.
    ''' </summary>
    Private Sub toolStripLast_Click(sender As Object, e As EventArgs) Handles toolStripLast.Click
        showRecord(dataTable.Length - 1)
    End Sub

    ''' <summary>
    ''' Event handler for the Toolstrip Search button.
    ''' </summary>
    Private Sub toolStripSearch_Click(sender As Object, e As EventArgs) Handles toolStripSearch.Click
        Dim strSearchTerm As String = InputBox("Search Term:", "Find Record")
        Dim index As Integer = findRecord(strSearchTerm)

        If index >= 0 Then
            showRecord(index)
        Else
            MsgBox("Record Not Found", MsgBoxStyle.Critical)
        End If
    End Sub

    ''' <summary>
    ''' Event handler for the File Menu Save button.
    ''' </summary>
    Private Sub fileMenuSave_Click(sender As Object, e As EventArgs) Handles fileMenuSave.Click
        saveRecord()

        If saveTable(strFilePathName, dataTable) = False Then
            MsgBox("Error writing data to file.", MsgBoxStyle.Critical)
        End If
    End Sub

    ''' <summary>
    ''' Event handler for the File Menu Open button.
    ''' </summary>
    Private Sub fileMenuOpen_Click(sender As Object, e As EventArgs) Handles fileMenuOpen.Click
        emptyRecords()
        strFilePathName = getFile()
        If strFilePathName <> Nothing Then
            fillTable(strFilePathName, dataTable)
            showRecord(0)
            toolStrip.Enabled = True
            fileMenuSave.Enabled = True
            Me.Text = "Memory Helper (" & strFilePathName & ")"
        End If
    End Sub

    ''' <summary>
    ''' Event handler for the File Menu New button.
    ''' </summary>
    Private Sub fileMenuNew_Click(sender As Object, e As EventArgs) Handles fileMenuNew.Click
        strFilePathName = newFile()
        If strFilePathName <> "" Then
            emptyRecords()
            updatePosition()
            toolStrip.Enabled = True
            fileMenuSave.Enabled = True
            Me.Text = "Memory Helper (" & strFilePathName & ")"
        Else
            MsgBox("A file was not created", MsgBoxStyle.Critical)
        End If
    End Sub

    ''' <summary>
    ''' Event handler for the File Menu Exit button.
    ''' </summary>
    Private Sub fileMenuExit_Click(sender As Object, e As EventArgs) Handles fileMenuExit.Click
        Close()
    End Sub

    Private Sub MainWindow_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        toolStrip.Enabled = False
        fileMenuSave.Enabled = False
        protectRecord()
    End Sub
End Class
